var keystone = require('keystone');
var jwt = require('jsonwebtoken');

exports = module.exports = function(req, res) {
    var view = new keystone.View(req, res),
        locals = res.locals;

    locals.section = 'chat';

    // Load other posts
    view.on('init', function(next) {
        
        var q = keystone.list('ChatMessage').model.find().sort('-publishedDate').populate('author').limit('50');
        
        q.exec(function(err, results) {
            locals.chatMessages = results;
            next(err);
        });
    });

    // we are sending the profile in the token
    if(locals.user) {
        locals.token = jwt.sign(locals.user, keystone.get('cookie secret'), { expiresInMinutes: 60*5 });
    } else {
        locals.csrf_token_key = keystone.security.csrf.TOKEN_KEY;
        locals.csrf_token_value = keystone.security.csrf.getToken(req, res);
    }

    // Render the view
    view.render('chat');
}