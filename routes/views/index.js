var keystone = require('keystone');

exports = module.exports = function(req, res) {
	
	var locals = res.locals,
		view = new keystone.View(req, res);
	
	// Set locals
	locals.section = 'home';
	
	req.flash('warning', {
		title: 'Still work in progress!',
		detail: 'It is still far from over...',
		icon: 'wrench'
	});

	req.flash('warning', {
    title: 'This page has insecure content!',
    detail: 'Because this site is meant for development purposes, \
    it doesn\'t have a valid SSL certificate and does not run over https protocol. \
    Please do NOT use passwords you use normally or pass sensitive information.',
    icon: 'attention'
  });

	// Render the view
	view.render('index');
	
}
