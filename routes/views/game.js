var keystone = require('keystone');

exports = module.exports = function(req, res) {
    
    var view = new keystone.View(req, res),
        locals = res.locals;

    locals.section = 'game';

    // Load the current post
    view.on('init', function(next) {
        //locals.navLinks.push({label: 'x', key:'test', href:'/'});
        next();
    });

    // Render the view
    view.render('game');

}