module.exports = function(sio, keystone) {

  var socketioJwt = require('socketio-jwt');

  var rooms = [];

  var usernames = [];

  var io = sio.of('/chat').use(socketioJwt.authorize({
    secret: keystone.get('cookie secret'),
    handshake: true
  }));
  io.on('connection', function (socket) {
    var addedUser = false;

    socket.join('user:'+socket.decoded_token.email);
    socket.to('user:'+socket.decoded_token.email).emit('chat message', socket.decoded_token.nickname);

    socket.on('chat message', function(msg){
      io.emit('chat message', {
        body: msg,
        user: {
          id: socket.decoded_token._id,
          nickname: socket.decoded_token.nickname,
          name: socket.decoded_token.name
        },
        gravatar: socket.decoded_token.gravatar
      });
    });

    // when the client emits 'add user', this listens and executes
    socket.on('add user', function (username) {
      // we store the username in the socket session for this client
      socket.username = username;
      // add the client's username to the global list
      usernames[username] = username;
      ++numUsers;
      addedUser = true;
      socket.emit('login', {
        numUsers: numUsers
      });
      // echo globally (all clients) that a person has connected
      socket.broadcast.emit('user joined', {
        username: socket.username,
        numUsers: numUsers
      });
    });

    // when the client emits 'typing', we broadcast it to others
    socket.on('typing', function () {
      socket.broadcast.emit('typing', {
        username: socket.username
      });
    });

    // when the client emits 'stop typing', we broadcast it to others
    socket.on('stop typing', function () {
      socket.broadcast.emit('stop typing', {
        username: socket.username
      });
    });

    // when the user disconnects.. perform this
    socket.on('disconnect', function () {
      // remove the username from global usernames list
      if (addedUser) {
        delete usernames[socket.username];
        --numUsers;

        // echo globally that this client has left
        socket.broadcast.emit('user left', {
          username: socket.username,
          numUsers: numUsers
        });
      }
    });
  });
}