// Using the module pattern for a jQuery feature
(function() {
  "use strict";
  jQuery.fn.extend({
    chat: (function() {

      var templates = {
        message: {
          body: '<div class="ui transition description">{{body}}</div>',
          user: '{{#gravatar}}<img class="ui avatar image top aligned" src="{{gravatar}}" />{{/gravatar}}'+
          '<div class="content">'+
            '<div class="header white">{{user}}</div>'+
          '</div>',
          item: '<div class="item"></div>'
        }
          
      };

      var chat = {
        window: null,
        connection: null,
        current_room: '',
        last_msg: null
      };

      var sounds = {
        message: new Audio('/sounds/message.mp3')
      };

      var connect_socket = function(token) {

        chat.connection = io.connect('/chat', {
          query: 'token=' + token
        });

        chat.connection
          .on('connect', function() {
            console.log('Connected!');
          })
          .on('disconnect', function() {
            console.log('Disconnected!');
          })
          .on('error', function(err) {
            console.log(err);
          })
          .on('chat message', function(msg){
            sounds.message.play();
            console.log(msg);
            if(chat.last_msg && chat.last_msg.user.id == msg.user.id) {
              var new_content = jQuery(Mustache.render(templates.message, msg));
              chat.last_msg.html.find('.content').append(new_content);
              // new_content.transition('fade down');
            } else {
              chat.last_msg = msg;
              chat.last_msg.html = jQuery(Mustache.render(templates.message_user, msg));
              chat.window.append(chat.last_msg.html);
            }
          });

        $('form#chat-input')
          .on('submit', function(e){
            chat.connection.emit('chat message', $('#m').val());
            $('#m').val('');
            e.preventDefault();
          })
          .on('click', '.button', function(e) {
            e.preventDefault();
            $(this).submit();
          });
      };

      var init =  function() {
        chat.window = this;
        connect_socket(token);
        return this;
      };

      return init;
    })()
  });
})();