jQuery(document).ready( function( $ ) {
	// create an new instance of a pixi stage
  // var stage = new PIXI.Stage(0x66FF99);

  // // create a renderer instance.
  // var renderer = PIXI.autoDetectRenderer(720, 480);

  // // add the renderer view element to the DOM
  // $('#viewport').append(renderer.view);

  // requestAnimFrame( animate );

  // // create a texture from an image path
  // var texture = PIXI.Texture.fromImage("images/bunny.png");
  // // create a new Sprite using the texture
  // var bunny = new PIXI.Sprite(texture);

  // // center the sprites anchor point
  // bunny.anchor.x = 0.5;
  // bunny.anchor.y = 0.5;

  // // move the sprite t the center of the screen
  // bunny.position.x = 360;
  // bunny.position.y = 240;

  // stage.addChild(bunny);

  // function animate() {

  //     requestAnimFrame( animate );

  //     // just for fun, lets rotate mr rabbit a little
  //     bunny.rotation += 0.01;

  //     // render the stage   
  //     renderer.render(stage);
  // }
/*
  var world = Physics();

  

  var renderer = Physics.renderer('canvas', {
    el: 'viewport', // The DOM element to append the stage to
    width: 720,
    height: 480,
    meta: true, // Turns debug info on/off,
    styles: {
        // set colors for the circle bodies
        'circle' : {
            strokeStyle: 'hsla(60, 37%, 17%, 1)',
            lineWidth: 1,
            fillStyle: 'hsla(60, 37%, 57%, 0.8)',
            angleIndicator: 'hsla(60, 37%, 17%, 0.4)'
        }
    }
	});

	// add the renderer
	world.add( renderer );

	var circle = Physics.body('circle', {
    x: 50, // x-coordinate
    y: 70, // y-coordinate
    vx: 0.2, // velocity in x-direction
    vy: 0.11, // velocity in y-direction,
    radius: 25
	});

	world.add( circle );

	// render on each step
	world.on('step', function(){
	    world.render();
	});

	*/

	Physics(function(world){

  var viewWidth = 720;
  var viewHeight = 480;

  var renderer = Physics.renderer('pixi', {
    el: 'viewport',
    width: viewWidth,
    height: viewHeight,
    meta: false, // don't display meta data
    styles: {
        // set colors for the circle bodies
        'circle' : {
            strokeStyle: '#351024',
            lineWidth: 1,
            fillStyle: '#d33682',
            angleIndicator: '#351024'
        }
    }
  });

  // add the renderer
  world.add( renderer );
  // render on each step
  world.on('step', function(){
    world.render();
  });

  // bounds of the window
  var viewportBounds = Physics.aabb(0, 0, viewWidth, viewHeight);

  // constrain objects to these bounds
  world.add(Physics.behavior('edge-collision-detection', {
      aabb: viewportBounds,
      restitution: 0.99,
      cof: 0.99
  }));

  // add a circle
  world.add(
      Physics.body('circle', {
        x: 50, // x-coordinate
        y: 30, // y-coordinate
        vx: 0.2, // velocity in x-direction
        vy: 0.01, // velocity in y-direction
        radius: 20
      })
  );

  // add some fun interaction
    var attractor = Physics.behavior('attractor', {
        order: 0,
        strength: 0.002
    });
    world.on({
        'interact:poke': function( pos ){
            attractor.position( pos );
            world.add( attractor );
        }
        ,'interact:move': function( pos ){
            attractor.position( pos );
        }
        ,'interact:release': function(){
            world.remove( attractor );
        }
    });

  // add things to the world
  world.add(Physics.behavior('interactive', { el: renderer.el }));

  // ensure objects bounce when edge collision is detected
  world.add( Physics.behavior('body-impulse-response') );

  // add some gravity
  world.add( Physics.behavior('constant-acceleration') );

  // subscribe to ticker to advance the simulation
  Physics.util.ticker.on(function( time, dt ){

      world.step( time );
  });

  // start the ticker
  Physics.util.ticker.start();

});
});

