var keystone = require('keystone'),
	Types = keystone.Field.Types;

var ChatMessage = new keystone.List('ChatMessage', {
	nocreate: true
});

ChatMessage.add({
	message: { type: String, required: true },
	author: { type: Types.Relationship, ref: 'User', index: true, required: true },
	publishedDate: { type: Types.Date, index: true },
	// room: { type: Types.Relationship, ref: 'PostCategory', many: true }
});

ChatMessage.register();
