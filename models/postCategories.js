var keystone = require('keystone'),
	Types = keystone.Field.Types;

var PostCategory = new keystone.List('PostCategory', {
	autokey: { from: 'name', path: 'key' }
});

PostCategory.add({
	name: { type: String, required: true },
  image: { type: Types.LocalFile, dest: 'public/uploads/category', prefix: '/uploads/category' }
});

PostCategory.relationship({ ref: 'Post', path: 'categories' });

PostCategory.register();
