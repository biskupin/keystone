var keystone = require('keystone'),
	Types = keystone.Field.Types;

var Gallery = new keystone.List('Gallery_local', {
	autokey: { from: 'name', path: 'key' }
});

Gallery.add({
	name: { type: String, required: true },
	publishedDate: { type: Date, default: Date.now },
	heroImage: { type: Types.LocalFile, dest: 'public/uploads/gallery', prefix: '/uploads/gallery' },
	images: { type: Types.LocalFile, dest: 'public/uploads/gallery', prefix: '/uploads/gallery' }
});

Gallery.register();
